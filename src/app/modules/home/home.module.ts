import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomePageComponent } from './home-page/home-page.component';
import { ReactiveFormsModule } from '@angular/forms';
import { HomeRoutingModule } from './home-routing.module';
import { HomeNewComponent } from './home-new/home-new.component';
import { JobDepartmentComponent } from './job-department/job-department.component';
import { HomeBestcompanyComponent } from './home-bestcompany/home-bestcompany.component';
import { HomeJobrecomComponent } from './home-jobrecom/home-jobrecom.component';
import { JobItComponent } from './job-it/job-it.component';
import { HomeArticleComponent } from './home-article/home-article.component';



@NgModule({
  declarations: [
    HomePageComponent,
    HomeNewComponent,
    JobDepartmentComponent,
    HomeBestcompanyComponent,
    HomeJobrecomComponent,
    JobItComponent,
    HomeArticleComponent
  ],
  imports: [
    CommonModule,
    HomeRoutingModule,
    ReactiveFormsModule
  ]
})
export class HomeModule { }
