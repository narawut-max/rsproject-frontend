import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { HomePageComponent } from './home-page/home-page.component';
import { HomeNewComponent } from './home-new/home-new.component';
import { HomeJobrecomComponent } from './home-jobrecom/home-jobrecom.component';
import { JobItComponent } from './job-it/job-it.component';
import { HomeArticleComponent } from './home-article/home-article.component';

const routes: Routes = [
  {
    path: '',
    component: HomeNewComponent,
  },
  {
    path: 'homepage',
    component: HomePageComponent,
  },
  {
    path: 'jobrecom',
    component: HomeJobrecomComponent,
  },
  {
    path: 'jobit',
    component: JobItComponent,
  },
  {
    path: 'article',
    component: HomeArticleComponent,
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
